package servlet;


import entity.Customer;
import lab2.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/authorization")
public class Authorization extends HttpServlet {

    private CustomerService customerService = new CustomerService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.removeAttribute("error");
        try {
            if (customerService.isExist(req.getParameter("login"), req.getParameter("password"))) {
                Customer customer = customerService.getByLoginPassword(req.getParameter("login"), req.getParameter("password"));
                req.getSession().setAttribute("customer", customer);
                req.setAttribute("customer", customer);
                resp.setCharacterEncoding("UTF-8");
                getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
                return;
            }
            req.getSession().setAttribute("error", "Wrong login or password!");
            resp.sendRedirect(req.getContextPath() + "/index.jsp");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
