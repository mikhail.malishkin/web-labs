package entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class ShoppingCart {

    private int id;
    private List<Good> goods;

    public ShoppingCart() {
    }

    public ShoppingCart(int id, List<Good> goods) {
        this.id = id;
        this.goods = goods;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlElement(name = "Good")
    @XmlElementWrapper(name = "Goods")
    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "id=" + id +
                ", goods=" + goods +
                '}';
    }

}

