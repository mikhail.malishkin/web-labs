<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ru">
<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <meta charset="UTF-8">
</head>
<body>
<div class="main">
    <table class="table">
        <caption><h1>Hello, ${customer.name}!</h1></caption>
        <tr>
            <th>Id</th>
            <th>Goods</th>
        </tr>
        <c:forEach var="shoppingCart" items="${customer.shoppingCarts}">
            <tr>
                <td>${shoppingCart.id}</td>
                <td>
                    <table>
                        <tr>
                            <th>Type</th>
                            <th>Price</th>
                            <th>Count</th>
                            <th>Cost</th>
                        </tr>
                        <c:forEach var="good" items="${shoppingCart.goods}">
                            <tr>
                                <td>${good.type}</td>
                                <td>${good.price}</td>
                                <td>${good.count}</td>
                                <td>${good.cost}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </c:forEach>
    </table>
    <h1><a href="result">xml output</a></h1>
</div>
</body>
</html>
