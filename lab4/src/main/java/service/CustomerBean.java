package service;

import entity.Customer;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;


@SessionScoped
@ManagedBean(name = "customerBean")
public class CustomerBean {

    @EJB
    private CustomerEJB customerEJB;
    private Customer customer = new Customer();
    private String error = "";

    public boolean validateUserLogin() throws SQLException {
        customer = customerEJB.validateUserLogin(customer.getLogin(), customer.getPassword());
        if (customer.getName() != null) {
            error = "";
            return true;
        }
        error = "Wrong login or password!";
        return false;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void viewXml() throws JAXBException, IOException {
        FacesContext ctx = FacesContext.getCurrentInstance();
        final HttpServletResponse resp = (HttpServletResponse) ctx.getExternalContext().getResponse();
        StringWriter writer = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        resp.setCharacterEncoding("UTF-8");
        marshaller.marshal(customer, writer);
        PrintWriter out = resp.getWriter();
        out.print(writer.toString());
        ctx.responseComplete();
    }

}
