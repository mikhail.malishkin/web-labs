package service;


import entity.Customer;
import lab2.CustomerService;

import javax.ejb.Stateless;
import java.sql.SQLException;

@Stateless
public class CustomerEJB {

    public Customer validateUserLogin(String login, String password) throws SQLException {
        CustomerService customerService = new CustomerService();
        if (customerService.isExist(login, password)) {
            return customerService.getByLoginPassword(login, password);
        }
        return new Customer();
    }

}
