package lab2;

import connect.Connect;
import entity.Good;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodService {

    private Connection connection;

    public GoodService() {
        this.connection = new Connect().getConnection();
    }

    public void add(Good good) throws SQLException {
        String sql = "INSERT INTO good VALUES (?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, good.getType());
            preparedStatement.setInt(2, good.getPrice());
            preparedStatement.setInt(3, good.getCount());
            preparedStatement.setInt(4, good.getCost());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Good get(String type) throws SQLException {
        String sql = "SELECT * FROM good WHERE type = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, type);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Good(
                        type,
                        resultSet.getInt("price"),
                        resultSet.getInt("count"),
                        resultSet.getInt("cost")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void update(Good good) throws SQLException {
        String sql = "UPDATE good SET price = ?, count = ?, cost = ? WHERE type = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement((sql))) {
            preparedStatement.setInt(1, good.getPrice());
            preparedStatement.setInt(2, good.getCount());
            preparedStatement.setInt(3, good.getCost());
            preparedStatement.setString(3, good.getType());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Good good) throws SQLException {
        String sql = "DELETE FROM good WHERE type = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, good.getType());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Good> getAll(int id) {
        List<Good> goods = new ArrayList<>();
        String sql = "SELECT * FROM shopping_cart_good left join good using (type) WHERE shopping_cart_id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                goods.add(new Good(
                        resultSet.getString("type"),
                        resultSet.getInt("price"),
                        resultSet.getInt("count"),
                        resultSet.getInt("cost")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return goods;
    }

}
