package lab2;

import connect.Connect;
import entity.ShoppingCart;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartService {

    private Connection connection;
    private GoodService goodService;

    public ShoppingCartService() {
        this.connection = new Connect().getConnection();
        this.goodService = new GoodService();
    }

    public void add(ShoppingCart shoppingCart, int index) throws SQLException {
        //
    }

    public ShoppingCart get(int id) throws SQLException {
        return new ShoppingCart(id, goodService.getAll(id));
    }

    public void update(ShoppingCart shoppingCart) throws SQLException {
        //
    }

    public void delete(ShoppingCart shoppingCart) throws SQLException {
        String sqlQuery = "DELETE FROM shopping_cart WHERE shopping_cart_id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            preparedStatement.setInt(1, shoppingCart.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<ShoppingCart> getAll(Integer id) {
        List<ShoppingCart> shoppingCarts = new ArrayList<>();
        String sql = "SELECT * FROM shopping_cart WHERE customer_id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                shoppingCarts.add(new ShoppingCart(
                        resultSet.getInt("shopping_cart_id"),
                        goodService.getAll(resultSet.getInt("shopping_cart_id"))
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return shoppingCarts;
    }

}
