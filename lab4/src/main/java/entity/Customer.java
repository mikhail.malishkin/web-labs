package entity;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@XmlRootElement
public class Customer {

    private int id;
    private String name;
    private String login;
    private String password;
    private List<ShoppingCart> shoppingCarts;

    public Customer() {
    }

    public Customer(int id, String name, String login, String password, List<ShoppingCart> shoppingCarts) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.shoppingCarts = shoppingCarts;
    }

    public void add(ShoppingCart shoppingCart) {
        shoppingCarts.add(shoppingCart);
    }

    public ShoppingCart get(int index) {
        return shoppingCarts.get(index);
    }

    public void remove(int index) {
        shoppingCarts.remove(index);
    }

    public int getSize() {
        return shoppingCarts.size();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    @XmlTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @XmlElement(name = "ShoppingCart")
    @XmlElementWrapper(name = "ShoppingCarts")
    public List<ShoppingCart> getShoppingCarts() {
        return shoppingCarts;
    }

    public void setShoppingCarts(List<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", shoppingCarts=" + shoppingCarts +
                '}';
    }

}
