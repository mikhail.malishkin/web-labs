package entity;

public class Good {

    private String type;
    private int price;
    private int count;
    private int cost;

    public Good() {
    }

    public Good(String type, int price, int count, int cost) {
        this.type = type;
        this.price = price;
        this.count = count;
        this.cost = cost;
    }

    public static Good stringToGood(String line) {
        String[] fields = line.split(",");
        return new Good(fields[0],  Integer.parseInt(fields[1]), Integer.parseInt(fields[2]), Integer.parseInt(fields[3]));
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Good{" +
                "type='" + type + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", cost=" + cost +
                '}';
    }

}
