import entity.Customer;
import entity.Good;
import entity.ShoppingCart;
import lab2.CustomerService;
import lab2.GoodService;
import lab2.ShoppingCartService;

import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {

        GoodService goodService = new GoodService();
        ShoppingCartService shoppingCartService = new ShoppingCartService();
        CustomerService customerService = new CustomerService();


        System.out.println("\nCheque operations: ");

        Good good = new Good("Manga", 1000, 203, 500);
        System.out.println("\nCreated good: " + good.toString());
        System.out.println("\nSaving good... ");
        goodService.add(good);
        System.out.println("Save operation complete. ");
        System.out.println("\nGetting good... ");
        System.out.println("Get operation complete: " + goodService.get("Manga").toString());
        good.setCost(0);
        good.setCount(0);
        good.setPrice(0);
        System.out.println("\nUpdating good... ");
        goodService.update(good);
        System.out.println("Update operation complete: " + goodService.get("Manga").toString());
        System.out.println("\nGetting all goods from Shopping Cart Id 100... ");
        List<Good> goods = goodService.getAll(101);
        for (Good temp : goods) {
            System.out.println(temp.toString());
        }
        System.out.println("Getting all operation complete. ");
        System.out.println("\nDeleting cheque... ");
        goodService.delete(good);
        System.out.println("Delete operation complete.");


        System.out.println("\nShoppingCart operations: ");

        ShoppingCart shoppingCart = new ShoppingCart(5, goods);
        System.out.println("\nCreated shopping cart: " + shoppingCart.toString());
        System.out.println("\nSaving shopping cart... ");
        shoppingCartService.add(shoppingCart, 1);
        System.out.println("Save operation complete. ");
        System.out.println("\nGetting shopping cart... ");
        System.out.println("Get operation complete: " + shoppingCartService.get(5).toString());
        System.out.println("\nGetting all shopping cart from customer 1... ");
        List<ShoppingCart> shoppingCarts = shoppingCartService.getAll(1);
        for (ShoppingCart temp : shoppingCarts) {
            System.out.println(temp.toString());
        }
        System.out.println("Getting all operation complete. ");
        System.out.println("\nDeleting chequeBook... ");
        shoppingCartService.delete(shoppingCart);
        System.out.println("Delete operation complete.");


        System.out.println("\nCustomer operations: ");

        Customer customer = new Customer(10, "Hikka", "Home", "Parol", shoppingCarts);
        System.out.println("\nCreated customer: " + customer.toString());
        System.out.println("\nSaving customer... ");
        customerService.add(customer);
        System.out.println("Save operation complete. ");
        System.out.println("\nGetting customer... ");
        System.out.println("Get operation complete: " + customerService.get(10).toString());
        customer.setName("imya");
        customer.setLogin("login");
        customer.setPassword("pass");
        System.out.println("\nUpdating customer... ");
        customerService.update(customer);
        System.out.println("Update operation complete: " + customerService.get(10).toString());
        System.out.println("\nChecking is exist... ");
        System.out.println("Checking operation complete: " + customerService.isExist("PogChamp", "Kappa"));
        System.out.println("\nGetting total paid... ");
        System.out.println("\nDeleting customer... ");
        customerService.delete(customer);
        System.out.println("Delete operation complete.");
    }

}
