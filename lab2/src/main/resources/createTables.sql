DROP TABLE IF EXISTS shopping_cart_good;
DROP TABLE IF EXISTS shopping_cart;

DROP TABLE IF EXISTS good;
DROP TABLE IF EXISTS customer;


CREATE TABLE good
(
    type  VARCHAR(255) PRIMARY KEY,
    price INTEGER,
    count INTEGER,
    cost  INTEGER
);

CREATE TABLE customer
(
    customer_id INTEGER PRIMARY KEY,
    name        VARCHAR(255),
    login       VARCHAR(255),
    password    VARCHAR(255)
);

CREATE TABLE shopping_cart
(
    shopping_cart_id INTEGER PRIMARY KEY,
    customer_id      INTEGER,
    FOREIGN KEY (customer_id) REFERENCES customer (customer_id)
);

CREATE TABLE shopping_cart_good
(
    shopping_cart_id INTEGER,
    type             VARCHAR(255),
    FOREIGN KEY (type) REFERENCES good (type),
    FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (shopping_cart_id)
);

INSERT INTO good
VALUES ('temp1', 2, 300, 11);
INSERT INTO good
VALUES ('temp2', 3, 120, 10);
INSERT INTO good
VALUES ('temp3', 5, 560, 15);
INSERT INTO good
VALUES ('temp4', 1, 330, 20);
INSERT INTO good
VALUES ('temp5', 7, 100, 12);
INSERT INTO good
VALUES ('temp6', 4, 70, 24);
INSERT INTO good
VALUES ('temp7', 6, 670, 13);
INSERT INTO good
VALUES ('temp8', 3, 440, 9);

INSERT INTO customer
VALUES (1, 'user1', 'fu', 'firstuser');
INSERT INTO customer
VALUES (2, 'user2', 'su', 'seconduser');
INSERT INTO customer
VALUES (3, 'user3', 'tu', 'thirduser');
INSERT INTO customer
VALUES (4, 'user4', 'fou', 'fourthuser');
INSERT INTO customer
VALUES (5, 'user5', 'fiu', 'fifthuser');

INSERT INTO shopping_cart
VALUES (101, 1);
INSERT INTO shopping_cart
VALUES (102, 1);
INSERT INTO shopping_cart
VALUES (201, 2);
INSERT INTO shopping_cart
VALUES (202, 2);
INSERT INTO shopping_cart
VALUES (301, 3);
INSERT INTO shopping_cart
VALUES (302, 3);
INSERT INTO shopping_cart
VALUES (401, 4);
INSERT INTO shopping_cart
VALUES (402, 4);
INSERT INTO shopping_cart
VALUES (501, 5);
INSERT INTO shopping_cart
VALUES (502, 5);


INSERT INTO shopping_cart_good
VALUES (101, 'temp1');
INSERT INTO shopping_cart_good
VALUES (101, 'temp2');
INSERT INTO shopping_cart_good
VALUES (101, 'temp6');
INSERT INTO shopping_cart_good
VALUES (101, 'temp4');
INSERT INTO shopping_cart_good
VALUES (101, 'temp8');
INSERT INTO shopping_cart_good
VALUES (102, 'temp3');
INSERT INTO shopping_cart_good
VALUES (102, 'temp6');
INSERT INTO shopping_cart_good
VALUES (102, 'temp2');
INSERT INTO shopping_cart_good
VALUES (102, 'temp1');
INSERT INTO shopping_cart_good
VALUES (102, 'temp8');
INSERT INTO shopping_cart_good
VALUES (201, 'temp3');
INSERT INTO shopping_cart_good
VALUES (201, 'temp2');
INSERT INTO shopping_cart_good
VALUES (201, 'temp7');
INSERT INTO shopping_cart_good
VALUES (201, 'temp1');
INSERT INTO shopping_cart_good
VALUES (201, 'temp5');
INSERT INTO shopping_cart_good
VALUES (202, 'temp4');
INSERT INTO shopping_cart_good
VALUES (202, 'temp2');
INSERT INTO shopping_cart_good
VALUES (202, 'temp1');
INSERT INTO shopping_cart_good
VALUES (202, 'temp6');
INSERT INTO shopping_cart_good
VALUES (202, 'temp3');
INSERT INTO shopping_cart_good
VALUES (301, 'temp8');
INSERT INTO shopping_cart_good
VALUES (301, 'temp7');
INSERT INTO shopping_cart_good
VALUES (301, 'temp2');
INSERT INTO shopping_cart_good
VALUES (301, 'temp5');
INSERT INTO shopping_cart_good
VALUES (301, 'temp1');
INSERT INTO shopping_cart_good
VALUES (302, 'temp6');
INSERT INTO shopping_cart_good
VALUES (302, 'temp3');
INSERT INTO shopping_cart_good
VALUES (302, 'temp2');
INSERT INTO shopping_cart_good
VALUES (302, 'temp5');
INSERT INTO shopping_cart_good
VALUES (302, 'temp4');
INSERT INTO shopping_cart_good
VALUES (401, 'temp1');
INSERT INTO shopping_cart_good
VALUES (401, 'temp8');
INSERT INTO shopping_cart_good
VALUES (401, 'temp3');
INSERT INTO shopping_cart_good
VALUES (401, 'temp2');
INSERT INTO shopping_cart_good
VALUES (401, 'temp5');
INSERT INTO shopping_cart_good
VALUES (402, 'temp1');
INSERT INTO shopping_cart_good
VALUES (402, 'temp8');
INSERT INTO shopping_cart_good
VALUES (402, 'temp6');
INSERT INTO shopping_cart_good
VALUES (402, 'temp3');
INSERT INTO shopping_cart_good
VALUES (402, 'temp5');
INSERT INTO shopping_cart_good
VALUES (501, 'temp4');
INSERT INTO shopping_cart_good
VALUES (501, 'temp2');
INSERT INTO shopping_cart_good
VALUES (501, 'temp3');
INSERT INTO shopping_cart_good
VALUES (501, 'temp1');
INSERT INTO shopping_cart_good
VALUES (501, 'temp2');
INSERT INTO shopping_cart_good
VALUES (502, 'temp7');
INSERT INTO shopping_cart_good
VALUES (502, 'temp4');
INSERT INTO shopping_cart_good
VALUES (502, 'temp6');
INSERT INTO shopping_cart_good
VALUES (502, 'temp8');
INSERT INTO shopping_cart_good
VALUES (502, 'temp3');
