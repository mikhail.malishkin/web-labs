import entities.ShoppingCart;
import services.SortService;

import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;


public class Client
{
    public static void main(String[] args)
    {
        String ip = "input.txt";
        String op = "output.txt";
        String inputPath, outputPath;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter input path");
        inputPath = in.nextLine();
        System.out.println("Enter output path");
        outputPath = in.nextLine();

        ShoppingCart cart = ShoppingCart.readFromFile(inputPath);

        try
        {
            Registry registry = LocateRegistry.getRegistry();
            SortService server = (SortService) registry.lookup("MessengerService");
            ShoppingCart sortedCart = server.sortShoppingCart(cart);

            System.out.println(cart.toString());
            System.out.println(sortedCart.toString());

            ShoppingCart.writeToFile(outputPath, sortedCart);

        }
        catch (RemoteException | NotBoundException e)
        {
            try {
                FileOutputStream fout = new FileOutputStream("error.txt");
                fout.write(e.toString().getBytes());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();

        }

        System.out.println("Hello world");
    }
}
