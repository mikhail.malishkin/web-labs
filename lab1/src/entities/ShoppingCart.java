package entities;

//import com.sun.tools.classfile.Opcode;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class ShoppingCart {

    private int id;
    private List<Good> goods;

    public ShoppingCart() {
    }

    public ShoppingCart(int id, List<Good> goods) {
        this.id = id;
        this.goods = goods;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }



    @Override
    public String toString() {
        return "ShoppingCart{" +
                "id=" + id +
                ", goods=" + goods +
                '}';
    }
    public static ShoppingCart readFromFile(String path)
    {
        try (FileInputStream fin = new FileInputStream(path))
        {
            List<Good> items = new ArrayList<Good>();
            byte[] bytes = fin.readAllBytes();
            String[] s = new String(bytes).split("\r\n");
            Integer number = Integer.parseInt(s[0]);

            for (int i = 1; i < s.length; ++i)
            {
                Good good = Good.stringToGood(s[i]);
                items.add(good);

            }
            ShoppingCart array = new ShoppingCart(number, items);
            return array;

        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
    public static void writeToFile(String path, ShoppingCart order)
    {
        try(FileOutputStream fout = new FileOutputStream(path))
        {
            String msg = order.toString();
            fout.write(msg.getBytes());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void sort() {
        Set<Good> ItemsWithoutDuplicate = new HashSet<>(goods);
        goods.clear();
        goods.addAll(ItemsWithoutDuplicate);
        goods.sort(Comparator.comparing(Good::getPrice));
    }
}

