package services;

import entities.ShoppingCart;

import java.rmi.Remote;
import java.rmi.RemoteException;



public interface SortService extends Remote
{
    String sendMessage(String message) throws RemoteException;
    ShoppingCart sortShoppingCart(ShoppingCart sortingShoppingCart) throws RemoteException;

}
