package services;


import entities.ShoppingCart;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class Server implements SortService {
    @Override
    public String sendMessage(String message) throws RemoteException {
        if (message.equals("Client message"))
            return "ServerMessage";
        else
            return null;
    }

    public ShoppingCart sortShoppingCart(ShoppingCart sortingOrder) throws RemoteException {
        sortingOrder.sort();
        return sortingOrder;
    }

    public static void main(String[] args)
    {
        Server messenger = new Server();
        messenger.createStubAndBind();
    }

    public void createStubAndBind()
    {
        SortService stub = null;
        try
        {
            stub = (SortService) UnicastRemoteObject.exportObject((SortService) this, 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("MessengerService", stub);
            System.out.println("Server ready!");
        }
        catch (RemoteException e)
        {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
